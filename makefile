rwildcard=$(foreach d,$(wildcard $(1:=/*)),$(call rwildcard,$d,$2) $(filter $(subst *,%,$2),$d))

BINDIR=bin
OBJDIR=$(BINDIR)/obj
SRCDIR=Optimisation

CC=gcc
CFLAGS=-W -pedantic
LDFLAGS=
MACROS=

SRC=$(call rwildcard,$(SRCDIR),*.c)
OBJ=$(notdir $(SRC:.c=.o))
#OBJ_IN_DIR=$(addprefix $(OBJDIR)/ga_,$(notdir $(SRC:.c=.o)))

EXEC_GLOUTON=greedy
EXEC_GENETICS=ga
EXEC_PSO=pso
EXEC_TABU=tabu

all: mrproper $(BINDIR)/greedy $(BINDIR)/ga $(BINDIR)/pso $(BINDIR)/tabu
greedy: mrproper $(BINDIR)/greedy
ga: mrproper $(BINDIR)/ga
pso: mrproper $(BINDIR)/pso
tabu: mrproper $(BINDIR)/tabu

$(BINDIR)/greedy: $(addprefix $(OBJDIR)/greedy_,$(notdir $(SRC:.c=.o))) 
	$(CC) -o $@ $^ -DGLOUTON $(CFLAGS) $(LDFLAGS)

$(BINDIR)/ga: $(addprefix $(OBJDIR)/ga_,$(notdir $(SRC:.c=.o)))
	$(CC) -o $@ $^ -DGENETICS $(CFLAGS) $(LDFLAGS)

$(BINDIR)/pso: $(addprefix $(OBJDIR)/pso_,$(notdir $(SRC:.c=.o)))
	$(CC) -o $@ $^ -DPSO $(CFLAGS) $(LDFLAGS)

$(BINDIR)/tabu: $(addprefix $(OBJDIR)/tabu_,$(notdir $(SRC:.c=.o)))
	$(CC) -o $@ $^ -DTABU $(CFLAGS) $(LDFLAGS)

$(OBJDIR)/greedy_%.o: $(SRCDIR)/%.c
	$(CC) -o $@ -c $< -DGLOUTON $(CFLAGS) $(LDFLAGS)
$(OBJDIR)/ga_%.o: $(SRCDIR)/%.c
	$(CC) -o $@ -c $< -DGENETICS $(CFLAGS) $(LDFLAGS)
$(OBJDIR)/pso_%.o: $(SRCDIR)/%.c
	$(CC) -o $@ -c $< -DPSO $(CFLAGS) $(LDFLAGS)
$(OBJDIR)/tabu_%.o: $(SRCDIR)/%.c
	$(CC) -o $@ -c $< -DTABU $(CFLAGS) $(LDFLAGS)
$(OBJDIR)/%.o: $(SRCDIR)/%.c
	$(CC) -o $@ -c $< $(CFLAGS) $(LDFLAGS)

mrproper: clean
	rm -f "$(BINDIR)/greedy"
	rm -f "$(BINDIR)/ga"
	rm -f "$(BINDIR)/pso"
	rm -f "$(BINDIR)/tabu"

clean:
	mkdir $(BINDIR) || exit 0;
	rm -rf "$(OBJDIR)" && mkdir $(OBJDIR)