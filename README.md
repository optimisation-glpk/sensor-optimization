# Sensor optimization

This project is part of an exam at ESIEA. The goal is to implement multiple optimization algorithms. 

Study case : Multiple targets must covered by sensors. Each sensor has a deployment cost. We must cover all the targets with the smallest deployment cost.

Four algorithms have been implemented : greedy algorithm, genetic algorithm, particle swarm algorithm and tabu search.

## Build

The project is compatible with every operating systems as long as they are shipped with STL. For now, two compilers are configured and ready to use, one on Windows, the other one on Linux.
If you want to use your own compiler, you simply need to specify a pre-defined macro, which refers to the algorithm that has to be built. Here are the macros to find according to the algorithm you want.

Algorithm | Macro
-|-
Greedy algorithm | `GLOUTON`
Genetic algorithm | `GENETICS`
Particle swarm algorithm | `PSO`
Tabu search | `TABU`

### Build on Windows

To build the program on Windows, you may use MSVC. You can generate an executable per algorithm by changing the defined macro on the top of the file `main.c`.

### Build on Linux

Run `make` in the root directory of the project. The executables will be built in the `bin` directory.

## Usage

The following command is the most basic one to run each program :
```
./algorithm execution_time input_file output_file
```
Some algorithms have specific arguments to change their parameters. They all have default values.

### Genetic algorithm
`-i`: Mix rate (Probability to mix two solutions)

`-u`: Mutation rate (Probabilty to change a part of a solution)

### Particle Swarm Algorithm

`-w`: Inertia

`-p`: Influence of the best solution found by the current particle

`-q`: Influence of the best solution found by the swarm

`-l`: Learning rate

### Tabu search

`-f`: Flips

`-k`: Number of neighbours compared
