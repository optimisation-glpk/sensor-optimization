#pragma once

#ifndef __INSTANCE_TO_LP_INCLUDED
#define __INSTANCE_TO_LP_INCLUDED

#include "Instance.h"

int LoadInstance(const char* instanceFile, Instance* output);

int SaveInstanceAsLP(Instance* instance, const char* dstFile);

#endif