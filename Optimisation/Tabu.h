#pragma once

#ifndef __TABU_DEF_INCLUDED
#define __TABU_DEF_INCLUDED

typedef struct TabuParameters {
	unsigned int flips;		// Nombre de modifications apport�es � la solution
	unsigned int solution_fork;			// Nombre de voisins g�n�r�s � partir de la solution
	unsigned char* start_solution;		// Solution de d�part
	unsigned int solution_size;			// Taille de la solution sp�cifi�e
	void (*repair)(unsigned char*, void*);			// Fonction de r�paration
	unsigned int (*eval)(unsigned char*, void*);	// Fonction d'�valutation
	int (*stop)(void*);	// Fonction avec condition d'arr�t
	void* userdata;		// Donn�es utilisateur
} TabuParameters;

/**
* \brief Ex�cute l'algorithme � liste taboue � partir de la solution sp�cifi�e
* \param params: Paramat�tres de l'algorithme
* \param solution [out]: Pointeur sur la solution
* \return Meilleure valeur objectif trouv�e
*/
unsigned int Tabu(TabuParameters params, unsigned char** solution);

#endif