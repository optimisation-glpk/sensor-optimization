#pragma once

#ifndef __GLOUTON_DEF_INCLUDED
#define __GLOUTON_DEF_INCLUDED

#include "Instance.h"

typedef struct {
	Instance* instance;
	unsigned char* targets_not_covered;
	unsigned char* sensors_enabled;
} GloutonInfo;

/**
* \brief Execute l'algorithme glouton sur l'instance sp�cifi�
* \param instance Instance � r�soudre
* \param f Heuristique
* \param sensors [out] Liste des capteurs activ�s
* \param nb_sensors [out] Nombre de capteurs activ�s
* \return Valeur de l'objectif, UINT_MAX si erreur
*/
unsigned int Glouton(Instance* i, unsigned int (*f)(GloutonInfo*), unsigned int** output, unsigned int* nb_sensors);

unsigned int GloutonAmeliore(Instance* i, unsigned int (*f)(GloutonInfo*), unsigned int** output, unsigned int* nb_sensors);

unsigned int glouton_heuristique(GloutonInfo* info);

unsigned int glouton_aleatoire(GloutonInfo* info);

#endif