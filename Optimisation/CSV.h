#pragma once

#ifndef __CSV_DEF_INCLUDED
#define __CSV_DEF_INCLUDED

#include <stdio.h>

typedef struct CSVRow {
	void* data;
	struct CSVDoc* doc;
	struct CSVRow* next;
} CSVRow;

typedef struct CSVDoc {
	unsigned int nb_columns;	// Number of columns
	char** columns;				// Column names
	CSVRow* _firstRow;			// First data row
} CSVDoc;

CSVDoc* CSV_CreateDoc(char** columns, unsigned int nb_columns);

CSVRow* CSV_Push(CSVDoc* doc, void* data);

CSVRow* CSV_InsertFirst(CSVDoc* doc, void* data);

int CSV_WriteDoc(CSVDoc* doc, const char* filepath, void(*printer)(FILE*,void*,unsigned int));

int CSV_FreeDoc(CSVDoc** doc, void(*freeData)(void*));

#endif