#pragma once

#ifndef __UTIL_DEF_INCLUDED
#define __UTIL_DEF_INCLUDED

int sum(unsigned char* array, unsigned int size);

unsigned int find(unsigned char* array, unsigned int size, unsigned char value);

float float_rand(float min, float max);

#endif