#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>

//#define GLOUTON
#define GENETICS
//#define PSO
//#define TABU

#include "CSV.h"
#include "InstanceIO.h"
#include "Timer.h"
#include "util.h"

#include "Glouton.h"
#if defined(GENETICS)
#include "Genetics.h"
#elif defined(PSO)
#include "PSO.h"
#elif defined(TABU)
#include "Tabu.h"
#endif

typedef struct {
	char* id;
	float value;
	unsigned char* sensors;
} CSV_DataRow;

// ----------- CSV -----------------

void CSV_FreeData(void* data) {
	CSV_DataRow* row = (CSV_DataRow*)data;
	if (row->id)
		free(row->id);
	if (row->sensors)
		free(row->sensors);
	free(row);
}

void CSV_PrintData(FILE* f, void* data, unsigned int i) {
	CSV_DataRow* row = (CSV_DataRow*)data;
	switch (i) {
		case 0:
			fprintf(f, "%s", row->id);
			break;
		case 1:
			fprintf(f, "%g", row->value);
			break;
		default:
			if (!row->sensors) return;
			i -= 2;
			fprintf(f, "%u", row->sensors[i]);
	};
}

// --------- Generic ---------------

struct Userdata {
	Instance* instance;
	Timer timer;
};

unsigned int EvaluateInstance(Instance* instance, unsigned char* solution) {
	if (!instance || !solution) return -1;
	unsigned int res = 0;
	for (unsigned int i = 0; i < instance->nb_sensors; i++)
		if (solution[i])
			res += instance->sensor_costs[i];
	return res;
}

int StopCondition(void* userdata) {
	Timer t = ((struct Userdata*)userdata)->timer;
	return !tfinished(t);
}

// --------- Genetics ---------------

#if defined(GENETICS)
int Genetics_Initializer(GeneticsSolution** solutions, unsigned int nb_solutions, void* userdata) {
	Instance* instance = ((struct Userdata*)userdata)->instance;

	unsigned int* sensors, nb_sensors, i, j;
	if (GloutonAmeliore(instance, &glouton_heuristique, &sensors, &nb_sensors) == (unsigned int)-1)
		return -1;

	assert(sensors);

	// 1 solution du glouton am�lior�
	solutions[0]->nb_chromosomes = instance->nb_sensors;
	solutions[0]->chromosomes = (unsigned char*)calloc(instance->nb_sensors, sizeof(unsigned char));
	assert(solutions[0]->chromosomes);
	for (i = 0; i < nb_sensors; i++)
		solutions[0]->chromosomes[sensors[i]] = 1;

	free(sensors);
	sensors = 0;

	// T-1 solutions du glouton al�atoire
	for (i = 1; i < nb_solutions; i++) {
		if (Glouton(instance, &glouton_aleatoire, &sensors, &nb_sensors) == 1)
			return -1;

		assert(sensors);

		solutions[i]->nb_chromosomes = instance->nb_sensors;
		solutions[i]->chromosomes = (unsigned char*)calloc(instance->nb_sensors, sizeof(unsigned char));
		assert(solutions[i]->chromosomes);
		for (j = 0; j < nb_sensors; j++)
			solutions[i]->chromosomes[sensors[j]] = 1;

		free(sensors);
		sensors = 0;
	}

	return 0;
}

unsigned int Genetics_Evaluate(GeneticsSolution* solution, void* userdata) {
	Instance* instance = ((struct Userdata*)userdata)->instance;
	unsigned int i, sum = 0;
	for (i = 0; i < solution->nb_chromosomes; i++)
		if (solution->chromosomes[i])
			sum += instance->sensor_costs[i];
	return sum;// EvaluateInstance(instance, solution->chromosomes);
}

void Genetics_Repair(GeneticsSolution* solution, void* userdata) {
	Instance* instance = ((struct Userdata*)userdata)->instance;

	unsigned int i, j;
	unsigned char* targets_not_covered = (unsigned char*)calloc(instance->nb_targets, sizeof(unsigned char));
	assert(targets_not_covered);

	for (i = 0; i < instance->nb_targets; i++)
		targets_not_covered[i] = 1;

	for (i = 0; i < solution->nb_chromosomes; i++)
		if (solution->chromosomes[i]) {
			for (j = 0; j < instance->nb_targets_covered[i]; j++)
				targets_not_covered[instance->targets_covered[i][j]] = 0;
		}

	unsigned int target, sensor_i, sensor_cost;
	while ((target = find(targets_not_covered, instance->nb_targets, 1)) != instance->nb_targets) {

		// Trouve le capteur le moins cher
		sensor_i = instance->sensors_covering[target][0];
		sensor_cost = instance->sensor_costs[instance->sensors_covering[target][0]];
		for (i = 1; i < instance->nb_sensors_covering[target]; i++)
			if (instance->sensor_costs[instance->sensors_covering[target][i]] < sensor_cost) {
				sensor_cost = instance->sensor_costs[instance->sensors_covering[target][i]];
				sensor_i = instance->sensors_covering[target][i];
			}

		// Active le premier capteur couvrant la cible
		// sensor_i = instance->sensors_covering[target][0];

		// Active le capteur
		solution->chromosomes[sensor_i] = 1;

		// Mis � jour des cibles non-couvertes
		for (i = 0; i < instance->nb_targets_covered[sensor_i]; i++)
			targets_not_covered[instance->targets_covered[sensor_i][i]] = 0;
	}

	free(targets_not_covered);
}

void Genetics_Free(GeneticsSolution* solution, void* userdata) {
	free(solution->chromosomes);
	solution->chromosomes = 0;
}
#endif

// ---------- PSO ------------------

#if defined(PSO)
void PSO_PosInitializer(PSOParticle** particles, unsigned int nb_particles, void* userdata) {
	Instance* instance = ((struct Userdata*)userdata)->instance;

	unsigned int* sensors, nb_sensors, i, j;
	if (GloutonAmeliore(instance, &glouton_heuristique, &sensors, &nb_sensors) == (unsigned int)-1)
		return;

	assert(sensors);

	// 1 solution du glouton am�lior�
	for (i = 0; i < nb_sensors; i++)
		particles[0]->position[sensors[i]] = 1;

	free(sensors);
	sensors = 0;

	// T-1 solutions du glouton al�atoire
	for (i = 1; i < nb_particles; i++) {
		if (Glouton(instance, &glouton_aleatoire, &sensors, &nb_sensors) == 1)
			return;

		assert(sensors);

		for (j = 0; j < nb_sensors; j++)
			particles[i]->position[sensors[j]] = 1;

		free(sensors);
		sensors = 0;
	}
}

void PSO_StateToSolution(PSOParticle* particle, unsigned char* solution, void* userdata) {
	Instance* instance = ((struct Userdata*)userdata)->instance;

	unsigned int i, j;

	// ----- Transformation des flottants en entiers et recherche des cibles déjà couvertes

	unsigned char* targets_not_covered = (unsigned char*)calloc(instance->nb_targets, sizeof(unsigned char));
	assert(targets_not_covered);

	for (i = 0; i < instance->nb_targets; i++)
		targets_not_covered[i] = 1;


	for (i = 0; i < particle->nb_components; i++)
		if (particle->position[i] > 0.7f) {
			solution[i] = 1;
			for (j = 0; j < instance->nb_targets_covered[i]; j++)
				targets_not_covered[instance->targets_covered[i][j]] = 0;
		}
		else
			solution[i] = 0;

	// ----- Réparation

	unsigned int target, sensor_i, sensor_cost;
	while ((target = find(targets_not_covered, instance->nb_targets, 1)) != instance->nb_targets) {

		// Trouve le capteur le moins cher
		sensor_i = instance->sensors_covering[target][0];
		sensor_cost = instance->sensor_costs[instance->sensors_covering[target][0]];
		for (i = 1; i < instance->nb_sensors_covering[target]; i++)
			if (instance->sensor_costs[instance->sensors_covering[target][i]] < sensor_cost) {
				sensor_cost = instance->sensor_costs[instance->sensors_covering[target][i]];
				sensor_i = instance->sensors_covering[target][i];
			}

		// Active le premier capteur couvrant la cible
		// sensor_i = instance->sensors_covering[target][0];

		// Active le capteur
		solution[sensor_i] = 1;

		// Mis à jour des cibles non-couvertes
		for (i = 0; i < instance->nb_targets_covered[sensor_i]; i++)
			targets_not_covered[instance->targets_covered[sensor_i][i]] = 0;
	}

	free(targets_not_covered);
}

unsigned int PSO_Evaluate(unsigned char* solution, void* userdata) {
	Instance* instance = ((struct Userdata*)userdata)->instance;
	return EvaluateInstance(instance, solution);
}
#endif

// --------- Tabu -------------------

#if defined(TABU)
unsigned int Tabu_Evaluate(unsigned char* solution, void* userdata) {
	if (!userdata || !solution) return -1;
	Instance* instance = ((struct Userdata*)userdata)->instance;
	return EvaluateInstance(instance, solution);
}

void Tabu_Repair(unsigned char* solution, void* userdata) {
	Instance* instance = ((struct Userdata*)userdata)->instance;

	unsigned int i, j;
	unsigned char* targets_not_covered = (unsigned char*)calloc(instance->nb_targets, sizeof(unsigned char));
	assert(targets_not_covered);

	for (i = 0; i < instance->nb_targets; i++)
		targets_not_covered[i] = 1;

	for (i = 0; i < instance->nb_sensors; i++)
		if (solution[i]) {
			for (j = 0; j < instance->nb_targets_covered[i]; j++)
				targets_not_covered[instance->targets_covered[i][j]] = 0;
		}

	unsigned int target, sensor_i, sensor_cost;
	while ((target = find(targets_not_covered, instance->nb_targets, 1)) != instance->nb_targets) {

		// Trouve le capteur le moins cher
		sensor_i = instance->sensors_covering[target][0];
		sensor_cost = instance->sensor_costs[instance->sensors_covering[target][0]];
		for (i = 1; i < instance->nb_sensors_covering[target]; i++)
			if (instance->sensor_costs[instance->sensors_covering[target][i]] < sensor_cost) {
				sensor_cost = instance->sensor_costs[instance->sensors_covering[target][i]];
				sensor_i = instance->sensors_covering[target][i];
			}

		// Active le premier capteur couvrant la cible
		// sensor_i = instance->sensors_covering[target][0];

		// Active le capteur
		solution[sensor_i] = 1;

		// Mis à jour des cibles non-couvertes
		for (i = 0; i < instance->nb_targets_covered[sensor_i]; i++)
			targets_not_covered[instance->targets_covered[sensor_i][i]] = 0;
	}

	free(targets_not_covered);
}
#endif

// --------- Main -------------------

int main(int argc, char** argv) {
	srand(clock());

	float time = 0.f;
	char* input = 0, * output = 0;
	unsigned int nb_res = 1;

#if defined(GENETICS)
	unsigned int nb = 200, nbs = 100;
	float mix = 1.f, mut = -1.f;
#elif defined(PSO)
	unsigned int nb = 200;
	float w = .5f, p = 2.f, q = .33f, lr = 1.f;
#elif defined(TABU)
	unsigned int f = 3, k = 2000;
#endif

	unsigned int i, j;
	char** curr_arg = argv + 1;
	for (i = 1, j = 0; i < (unsigned int)argc; i++, curr_arg++) {
		if (!strcmp("-n", *curr_arg)) {
			i++;
			if (i >= (unsigned int)argc) {
				fprintf(stderr, "Format incorrect : -n <nb_res>\n");
				return 1;
			}
			curr_arg++;
			if (!sscanf(*curr_arg, "%u", &nb_res) || nb_res == 0) {
				fprintf(stderr, "Valeur de l'option -n invalide : %s\n", *curr_arg);
				return 1;
			}
		}
#if defined(GENETICS)
		else if (!strcmp("-i", *curr_arg)) {
			i++;
			if (i >= (unsigned int)argc) {
				fprintf(stderr, "Format incorrect : -i <taux>\n");
				return 1;
			}
			curr_arg++;
			if (!sscanf(*curr_arg, "%f", &mix) || mix < 0.f || mix > 1.f) {
				fprintf(stderr, "Valeur de l'option -w invalide : %s\n", *curr_arg);
				return 1;
			}
		}
		else if (!strcmp("-u", *curr_arg)) {
			i++;
			if (i >= (unsigned int)argc) {
				fprintf(stderr, "Format incorrect : -u <taux>\n");
				return 1;
			}
			curr_arg++;
			if (!sscanf(*curr_arg, "%f", &mut) || mut < 0.f || mut > 1.f) {
				fprintf(stderr, "Valeur de l'option -u invalide : %s\n", *curr_arg);
				return 1;
			}
		}
		else if (!strcmp("-c", *curr_arg)) {
			i++;
			if (i >= (unsigned int)argc) {
				fprintf(stderr, "Format incorrect : -c <nombre>\n");
				return 1;
			}
			curr_arg++;
			if (!sscanf(*curr_arg, "%u", &nb)) {
				fprintf(stderr, "Valeur de l'option -c invalide : %s\n", *curr_arg);
				return 1;
			}
		}
		else if (!strcmp("-s", *curr_arg)) {
			i++;
			if (i >= (unsigned int)argc) {
				fprintf(stderr, "Format incorrect : -s <nombre>\n");
				return 1;
			}
			curr_arg++;
			if (!sscanf(*curr_arg, "%u", &nbs)) {
				fprintf(stderr, "Valeur de l'option -s invalide : %s\n", *curr_arg);
				return 1;
			}
		}
#elif defined(PSO)
		else if (!strcmp("-w", *curr_arg)) {
			i++;
			if (i >= (unsigned int)argc) {
				fprintf(stderr, "Format incorrect : -w <inertie>\n");
				return 1;
			}
			curr_arg++;
			if (!sscanf(*curr_arg, "%f", &w) || w < 0) {
				fprintf(stderr, "Valeur de l'option -w invalide : %s\n", *curr_arg);
				return 1;
			}
		}
		else if (!strcmp("-p", *curr_arg)) {
			i++;
			if (i >= (unsigned int)argc) {
				fprintf(stderr, "Format incorrect : -p <p1>\n");
				return 1;
			}
			curr_arg++;
			if (!sscanf(*curr_arg, "%f", &p) || p < 0) {
				fprintf(stderr, "Valeur de l'option -p invalide : %s\n", *curr_arg);
				return 1;
			}
		}
		else if (!strcmp("-q", *curr_arg)) {
			i++;
			if (i >= (unsigned int)argc) {
				fprintf(stderr, "Format incorrect : -q <p2>\n");
				return 1;
			}
			curr_arg++;
			if (!sscanf(*curr_arg, "%f", &q) || q < 0) {
				fprintf(stderr, "Valeur de l'option -q invalide : %s\n", *curr_arg);
				return 1;
			}
		}
		else if (!strcmp("-l", *curr_arg)) {
			i++;
			if (i >= (unsigned int)argc) {
				fprintf(stderr, "Format incorrect : -l <vitesse>\n");
				return 1;
			}
			curr_arg++;
			if (!sscanf(*curr_arg, "%f", &lr) || lr < 0) {
				fprintf(stderr, "Valeur de l'option -l invalide : %s\n", *curr_arg);
				return 1;
			}
		}
		else if (!strcmp("-c", *curr_arg)) {
		i++;
		if (i >= (unsigned int)argc) {
			fprintf(stderr, "Format incorrect : -c <nombre>\n");
			return 1;
		}
		curr_arg++;
		if (!sscanf(*curr_arg, "%u", &nb)) {
			fprintf(stderr, "Valeur de l'option -c invalide : %s\n", *curr_arg);
			return 1;
		}
		}
#elif defined(TABU)
		else if (!strcmp("-f", *curr_arg)) {
			i++;
			if (i >= (unsigned int)argc) {
				fprintf(stderr, "Format incorrect : -f <flips>\n");
				return 1;
			}
			curr_arg++;
			if (!sscanf(*curr_arg, "%u", &f) || f == 0) {
				fprintf(stderr, "Valeur de l'option -f invalide : %s\n", *curr_arg);
				return 1;
			}
		}
		else if (!strcmp("-k", *curr_arg)) {
			i++;
			if (i >= (unsigned int)argc) {
				fprintf(stderr, "Format incorrect : -k <neighbours>\n");
				return 1;
			}
			curr_arg++;
			if (!sscanf(*curr_arg, "%u", &k) || k == 0) {
				fprintf(stderr, "Valeur de l'option -k invalide : %s\n", *curr_arg);
				return 1;
			}
		}
#endif
		else {
			switch (j) {
			case 0:
				if (!sscanf(*curr_arg, "%f", &time) || time < 0) {
					fprintf(stderr, "Argument de temps invalide: %s\n", *curr_arg);
					return 1;
				}
				break;
			case 1:
				input = *curr_arg;
				break;
			case 2:
				output = *curr_arg;
				break;
			default:
				printf("Argument inconnu : %s\n", *curr_arg);
			}
			j++;
		}
	}

	if (time <= 0) {
		fprintf(stderr, "Veuillez préciser le temps d'exécution\n");
		return 1;
	}
	else if (!input) {
		fprintf(stderr, "Veuillez préciser le fichier d'entrée\n");
		return 1;
	}
	else if (!output) {
		fprintf(stderr, "Veuillez préciser le fichier de sortie\n");
		return 1;
	}

	Instance* instance;

	instance = AllocInstance();
	if (LoadInstance(input, instance)) {
		fprintf(stderr, "Impossible de lire le fichier d'entrée spécifié\n");
		return 1;
	}

	CSVDoc* doc = 0;
	if (nb_res > 1) {
		char** columns = (char**)calloc(2 + (size_t)instance->nb_sensors, sizeof(char*));
		assert(columns);
		columns[0] = "Numéro";
		columns[1] = "Valeur objectif";
		char* buffer = 0;
		for (i = 1; i <= instance->nb_sensors; i++) {
			buffer = (char*)calloc(15, sizeof(char));
			assert(buffer);
			sprintf(buffer, "Capteur %u", i);
			columns[1 + i] = buffer;
		}
		doc = CSV_CreateDoc(columns, 2 + instance->nb_sensors);
		for (i = 0; i < instance->nb_sensors; i++)
			free(columns[2 + i]);
	}

	unsigned int* sensors = 0, nb_sensors = 0, cost = 0;
	unsigned char* solution = 0;

	struct Userdata userdata = {
		instance,
		tcreate()
	};

	CSV_DataRow* min_row = 0, * max_row = 0, * avg_row = 0;
	if (nb_res > 1) {
		min_row = (CSV_DataRow*)malloc(sizeof(CSV_DataRow));
		max_row = (CSV_DataRow*)malloc(sizeof(CSV_DataRow));
		avg_row = (CSV_DataRow*)malloc(sizeof(CSV_DataRow));
		min_row->id = (char*)calloc(8, sizeof(char));
		max_row->id = (char*)calloc(8, sizeof(char));
		avg_row->id = (char*)calloc(8, sizeof(char));
		memcpy(min_row->id, "Minimum\0", 8);
		memcpy(max_row->id, "Maximum\0", 8);
		memcpy(avg_row->id, "Moyenne\0", 8);
		min_row->value = -1;
		max_row->value = 0;
		avg_row->value = 0;
		min_row->sensors = 0;
		max_row->sensors = 0;
		avg_row->sensors = 0;
	}

	for (i = 0; i < nb_res; i++) {
		tstart(userdata.timer, (float)time);

		if (nb_res > 1)
			printf("En cours de traitement... (%u/%u)\n", i + 1, nb_res);

#if defined(GLOUTON)
		cost = GloutonAmeliore(instance, &glouton_heuristique, &sensors, &nb_sensors);
		solution = (unsigned char*)calloc(instance->nb_sensors, sizeof(unsigned char));
		assert(solution);
		for (i = 0; i < nb_sensors; i++)
			solution[sensors[i]] = 1;
#elif defined(GENETICS)
		GeneticsInstance* g_instance = Genetics_CreateInstance(nb, nbs);
		Genetics_SetUserData(g_instance, &userdata);
		Genetics_SetInitializer(g_instance, &Genetics_Initializer);
		Genetics_SetSolutionEvaluator(g_instance, &Genetics_Evaluate);
		Genetics_SetSolutionRepair(g_instance, &Genetics_Repair);
		Genetics_SetFreeSolution(g_instance, &Genetics_Free);
		Genetics_SetStopCondition(g_instance, &StopCondition);
		Genetics_SetMixProbability(g_instance, mix);
		Genetics_SetMutationProbability(g_instance, (mut < 0) ? 2.f / instance->nb_sensors : mut);
		cost = Genetics_Process(g_instance, &solution);
		Genetics_FreeInstance(&g_instance);
#elif defined(PSO)
		PSOInstance* p_instance = PSO_CreateInstance(nb, instance->nb_sensors);
		PSO_SetInertia(p_instance, w);
		PSO_SetSelfRate(p_instance, p);
		PSO_SetSwarmRate(p_instance, q);
		PSO_SetLearningRate(p_instance, lr);
		PSO_SetStateToSolution(p_instance, &PSO_StateToSolution);
		PSO_SetEvaluationFunction(p_instance, &PSO_Evaluate);
		PSO_SetPosInitFunction(p_instance, &PSO_PosInitializer);
		PSO_SetStopCondition(p_instance, &StopCondition);
		PSO_SetUserData(p_instance, &userdata);
		cost = PSO_Process(p_instance, &solution);
		PSO_FreeInstance(&p_instance);
#elif defined(TABU)
		cost = GloutonAmeliore(instance, &glouton_heuristique, &sensors, &nb_sensors);

		unsigned char* enabled_sensors = (unsigned char*)calloc(instance->nb_sensors, sizeof(unsigned char));
		assert(enabled_sensors);
		for (j = 0; j < nb_sensors; j++)
			enabled_sensors[sensors[j]] = 1;

		TabuParameters tparams;
		tparams.flips = f;
		tparams.solution_fork = k;
		tparams.start_solution = enabled_sensors;
		tparams.solution_size = instance->nb_sensors;
		tparams.repair = &Tabu_Repair;
		tparams.eval = &Tabu_Evaluate;
		tparams.stop = &StopCondition;
		tparams.userdata = &userdata;
		cost = Tabu(tparams, &solution);

		free(enabled_sensors);
#endif

		if (nb_res > 1) {
			avg_row->value += cost;

			if (cost > max_row->value)
				max_row->value = cost;
			if (cost < min_row->value)
				min_row->value = cost;
			else if (min_row->value == -1.f)
				min_row->value = cost;

			CSV_DataRow* row = (CSV_DataRow*)malloc(sizeof(CSV_DataRow));
			assert(row);

			char* id = (char*)calloc(6, sizeof(char));
			assert(id);

			sprintf(id, "%d", i + 1);
			row->id = id;
			row->value = cost;
			row->sensors = (unsigned char*)calloc(instance->nb_sensors, sizeof(unsigned char));
			assert(row->sensors);
			if (solution)
				for (j = 0; j < instance->nb_sensors; j++)
					row->sensors[j] = solution[j];
			CSV_Push(doc, row);
		} else if (solution) {
			FILE* f = fopen(output, "w");
			if (!f) {
				fprintf(stderr, "Impossible d'ouvrir le fichier %s\n", output);
				return 1;
			}
			for (j = 0; j < instance->nb_sensors; j++)
				fprintf(f, "%u ", solution[j]);
			fclose(f);
		}

		if (solution)
			free(solution);
	}

	if (nb_res > 1) {
		avg_row->value /= nb_res;

		CSV_InsertFirst(doc, avg_row);
		CSV_InsertFirst(doc, max_row);
		CSV_InsertFirst(doc, min_row);

	} else
		printf("%u\n", cost);


	if (FreeInstance(&instance)) {
		fprintf(stderr, "Impossible de libérer la mémoire de l'instance\n");
		return 1;
	}

	if (nb_res > 1) {
		if (CSV_WriteDoc(doc, output, &CSV_PrintData)) {
			fprintf(stderr, "Impossible d'écrire dans le fichier de sortie\n");
			return 1;
		}
		else
			printf("Résultats stockés au format CSV dans le fichier %s\n", output);
		CSV_FreeDoc(&doc, &CSV_FreeData);
	}

	return 0;
}