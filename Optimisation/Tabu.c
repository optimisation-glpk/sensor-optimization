#include "Tabu.h"

#include <stdlib.h>
#include <assert.h>

struct Tabu {
	TabuParameters param;		// Param�tres de l'algorithme
	unsigned char* current;		// Solution actuelle
	unsigned int depth;			// Profondeur de recherche actuelle
};

int contains(unsigned int* list, unsigned int list_size, unsigned value) {
	for (unsigned int i = 0; i < list_size; i++)
		if (list[i] == value)
			return 1;
	return 0;
}

unsigned int Tabu(TabuParameters param, unsigned char** best_solution)
{
	if (!param.start_solution || param.solution_size <= 0 || param.solution_fork <= 0 || !param.eval || !param.repair) return -1;

	unsigned int i, j, k = 0, r;
	unsigned int depth = 0, best_neigh_score, best_score, nb_forbidden_moves = 7 * param.flips;
	unsigned char* current = (unsigned char*)calloc(param.solution_size, sizeof(unsigned char)),
		*best_neigh, 
		*neigh;
	unsigned int* forbidden_moves = (unsigned int*)calloc(nb_forbidden_moves, sizeof(unsigned int)), 
		*moves_done = (unsigned int*)calloc(param.flips, sizeof(unsigned int)),
		*best_neigh_moves = (unsigned int*)calloc(param.flips, sizeof(unsigned int));
	if (best_solution) {
		*best_solution = (unsigned char*)calloc(param.solution_size, sizeof(unsigned char));
		assert(*best_solution);
	}

	assert(current);
	assert(forbidden_moves);
	assert(moves_done);

	// Copy start solution
	for (i = 0; i < param.solution_size; i++) {
		current[i] = param.start_solution[i];
		if (best_solution)
			(*best_solution)[i] = param.start_solution[i];
	}
	best_score = param.eval(current, param.userdata);

	while ((param.stop) ? param.stop(param.userdata) : depth++ < 20) {
		best_neigh_score = -1;
		best_neigh = 0;
		for (i = 0; i < param.solution_fork; i++) {
			neigh = (unsigned char*)calloc(param.solution_size, sizeof(unsigned char));
			assert(neigh);

			// Copie de la solution courante dans le voisin
			for (j = 0; j < param.solution_size; j++)
				neigh[j] = current[j];

			// G�n�ration du voisin
			for (j = 0; j < param.flips; j++) {
				do {
					r = rand() % param.solution_size;
				} while (contains(forbidden_moves, nb_forbidden_moves, r));
				moves_done[j] = r;
				neigh[r] = !neigh[r];
			}

			param.repair(neigh, param.userdata);	// R�paration pour avoir une solution

			// S�lection du meilleur voisin
			if ((r = param.eval(neigh, param.userdata)) < best_neigh_score) {
				best_neigh_score = r;
				if (best_neigh)
					free(best_neigh);
				best_neigh = neigh;
				for (j = 0; j < param.flips; j++)
					best_neigh_moves[j] = moves_done[j];
			}
			else
				free(neigh);
		}

		// Les mouvements r�alis�s sont maintenant interdits
		for (i = 0; i < param.flips; i++)
			forbidden_moves[k + i] = best_neigh_moves[i];
		k = (k + param.flips) % nb_forbidden_moves;

		// Voisins = meilleure solution ?
		if (best_neigh_score < best_score) {
			best_score = best_neigh_score;
			if (best_solution) {
				for (i = 0; i < param.solution_size; i++)
					(*best_solution)[i] = best_neigh[i];
			}
		}

		free(current);
		current = best_neigh;
	}

	free(current);
	free(forbidden_moves);
	free(moves_done);
	free(best_neigh_moves);

	return best_score;
}
