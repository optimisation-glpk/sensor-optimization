#pragma once

#ifndef __INSTANCE_DEF_INCLUDED
#define __INSTANCE_DEF_INCLUDED

typedef struct {
	unsigned int nb_sensors, nb_targets;	// Nombre de capteurs et de cibles
	unsigned int* sensor_costs;				// Co�t de chaque capteur
	unsigned int** targets_covered;			// targets_covered[i] : Liste des cibles couvertes par le capteur i
	unsigned int* nb_targets_covered;		// Nombre de de cibles couverte par le capteur i
	unsigned int** sensors_covering;		// sensors_covering[i] : Liste des capteurs couvrant la cible i
	unsigned int* nb_sensors_covering;		// Nombre de capteurs couvrant la cible i
	unsigned char** targets_sensors;		// Matrice[cible][capteur] : 1 si capteur couvre cible, 0 sinon
} Instance;

Instance* AllocInstance();

int FreeInstance(Instance** instance);

#endif