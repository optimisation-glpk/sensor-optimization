#pragma once

#ifndef __PSO_DEF_INCLUDED
#define __PSO_DEF_INCLUDED

typedef struct PSOParticle {
	unsigned int nb_components;	// Nombre de composantes dans l'espace des solutions
	float* velocity;			// V�locit� de la particule
	float* position;			// Position de la particule
	struct PSOInstance* instance;	// Instance du probl�me
} PSOParticle;

typedef struct PSOInstance PSOInstance;

/**
* \brief Cr�er une instance d'ex�cution pour l'algorithme � essaim particuli�re
* \param nb_particles: Nombre de particules
* \param nb_components: Dimension de l'espace des solutions
* \return Pointeur sur l'instance, NULL si erreur
*/
PSOInstance* PSO_CreateInstance(unsigned int nb_particles, unsigned int nb_components);

/**
* \brief Copie une particule
* \param particle: Particule � copier
* \return Copie de la particule
*/
PSOParticle* PSO_CopyParticle(PSOParticle* particle);

/**
* \brief D�finit l'inertie des particules
* \param instance: Instance d'ex�cution
* \param w: Inertie (0 < influence < 1)
* \return 0 si r�ussi, 1 sinon
*/
int PSO_SetInertia(PSOInstance* instance, float w);

/**
* \brief D�finit l'infuence de la meilleure solution trouv�e par la particule
* \param instance: Instance d'ex�cution
* \param r: Influence (0 < influence < 1)
* \return 0 si r�ussi, 1 sinon
*/
int PSO_SetSelfRate(PSOInstance* instance, float r);

/**
* \brief D�finit l'infuence de la meilleure solution trouv�e par l'essaim
* \param instance: Instance d'ex�cution
* \param r: Influence (0 < influence < 1)
* \return 0 si r�ussi, 1 sinon
*/
int PSO_SetSwarmRate(PSOInstance* instance, float r);

/**
* \brief D�finit la vitesse de l'essaim
* \param instance: Instance d'ex�cution
* \param r: Vitesse
* \return 0 si r�ussi, 1 sinon
*/
int PSO_SetLearningRate(PSOInstance* instance, float r);

/**
* \brief Definit la fonction de passage d'�tat � une solution
* \param instance: Instance d'ex�cution
* \param stateToSolution: Fonction de transformation
* \return 0 si r�ussi, 1 sinon
*/
int PSO_SetStateToSolution(PSOInstance* instance, void (*stateToSolution)(PSOParticle*, unsigned char*, void*));

/**
* \brief D�finit la fonction d'�valuation d'une particule
* \param instance: Instance d'ex�cution
* \param eval: Fonction d'�valuation
* \return 0 si r�ussi, 1 sinon
*/
int PSO_SetEvaluationFunction(PSOInstance* instance, unsigned int (*eval)(unsigned char*,void*));

/**
* \brief D�finit la fonction d'initialisation de la position des particules
* \param instance: Instance d'ex�cution
* \param eval: Fonction d'initialisation
* \return 0 si r�ussi, 1 sinon
*/
int PSO_SetPosInitFunction(PSOInstance* instance, void (*pos_init)(PSOParticle**, unsigned int, void*));

/**
* \brief D�finit la condition d'arr�t
* \param instance: Instance d'ex�cution
* \param stop: Fonction avec condition d'arr�t
* \return 0 si r�ussi, 1 sinon
*/
int PSO_SetStopCondition(PSOInstance* instance, int (*stop)(void*));

/**
* \brief D�finit les donn�es utilisateurs
* \param instance: Instance d'ex�cution
* \param userdata, Donn�es utilisateurs
* \return 0 si r�ussi, 1 sinon
*/
int PSO_SetUserData(PSOInstance* instance, void* userdata);

/**
* \brief Ex�cute l'instance et retourne la meilleur valeur objectif atteinte
* \param instance: Instance d'ex�cution
* \param nb_iterations: Nombre d'it�rations
* \param solution: Pointeur sur la solution
* \return Valeur objectif
*/
unsigned int PSO_Process(PSOInstance* instance, unsigned char** solution);

/**
* \brief Lib�re l'instance d'ex�cution
* \param instance: Pointeur sur l'instance d'ex�cution
* \return 0 si r�ussi, 1 sinon
*/
int PSO_FreeInstance(PSOInstance** instance);

/**
* \brief Lib�re une particule
* \param particle: Pointeur sur la particule � lib�rer
* \return 0 si r�ussi, 1 sinon
*/
int PSO_FreeParticle(PSOParticle** particle);

#endif