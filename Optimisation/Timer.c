#include "Timer.h"

#include <stdlib.h>
#include <assert.h>
#include <time.h>

struct Timer {
	clock_t start_clock;
	float time;
};

Timer tcreate()
{
	Timer t = (Timer)malloc(sizeof(struct Timer));
	assert(t);
	t->start_clock = 0;
	t->time = 0;
	return t;
}

int tstart(Timer timer, float time)
{
	if (!timer || time <= 0) return -1;
	timer->start_clock = clock();
	timer->time = time;
	return 0;
}

float tremaining(Timer timer)
{
	if (!timer) return 0.f;
	float res = timer->time - (clock() - timer->start_clock) / (float)CLOCKS_PER_SEC;
	if (res < 0.f)
		res = 0.f;
	return res;
}

int tfinished(Timer timer)
{
	if (!timer) return 1;
	return tremaining(timer) <= 0.f;
}

int tfree(Timer* timer)
{
	if (!timer || !*timer) return -1;
	free(*timer);
	*timer = 0;
	return 0;
}


