#include "InstanceIO.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define INSTANCEIO_BUFFER_RESIZE 20

int LoadInstance(const char* instanceFile, Instance* output) {
	if (!output) return 1;

	FILE* f = fopen(instanceFile, "r");	// Ouverture du fichier d'instance

	if (!f) return 1;

	unsigned int i, j;	// It�rateurs

	// unsigned int M = 0, N = 0;	// M: Nombre de cibles ; N: Nombre de capteurs
	// unsigned int* cost;	// Liste des co�ts des capteurs
	// unsigned char** targets_sensors;	// Matrice[cible][capteur] : capteurs couvrant cibles

	{	// Parsing
		assert(fscanf(f, " %u %u \n", &output->nb_targets, &output->nb_sensors));	// Si aucune donn�e n'est lue, c'est pas bon
		// printf("M = %u\nN = %u\n", M, N);

		output->sensor_costs = (unsigned int*)malloc(sizeof(unsigned int) * output->nb_sensors);
		assert(output->sensor_costs);

		output->targets_covered = (unsigned int**)calloc(output->nb_sensors, sizeof(unsigned int*));
		assert(output->targets_covered);

		output->nb_targets_covered = (unsigned int*)calloc(output->nb_sensors, sizeof(unsigned int));
		assert(output->nb_targets_covered);

		// for (i = 0; i < output->nb_sensors; i++) {
		// 	output->targets_covered[i] = (unsigned int*)calloc(20, sizeof(unsigned int));
		// 	assert(output->targets_covered[i]);
		// }

		output->sensors_covering = (unsigned int**)calloc(output->nb_targets, sizeof(unsigned int*));
		assert(output->sensors_covering);

		output->nb_sensors_covering = (unsigned int*)calloc(output->nb_targets, sizeof(unsigned int));
		assert(output->nb_sensors_covering);

		output->targets_sensors = (unsigned char**)calloc(output->nb_targets, sizeof(unsigned char*));
		assert(output->targets_sensors);

		for (i = 0; i < output->nb_targets; i++) {
			output->targets_sensors[i] = (unsigned char*)calloc(output->nb_sensors, sizeof(unsigned char));
			assert(output->targets_sensors[i]);
		}

		for (i = 0, j = 0; i < output->nb_sensors; i++, j++) {
			assert(fscanf(f, "%u", output->sensor_costs + i));
			// printf("%u ", cost[i]);
		}

		unsigned int nb_sensors = 0;
		unsigned int sensor_i = 0;
		for (i = 0; i < output->nb_targets; i++) {
			assert(fscanf(f, "%u", &nb_sensors));
			output->sensors_covering[i] = (unsigned int*)calloc(nb_sensors, sizeof(unsigned int));
			output->nb_sensors_covering[i] = nb_sensors;
			for (j = 0; j < nb_sensors; j++) {
				assert(fscanf(f, "%u", &sensor_i));
				assert(--sensor_i < output->nb_sensors);
				output->targets_sensors[i][sensor_i] = 1;
				output->sensors_covering[i][j] = sensor_i;

				if (output->nb_targets_covered[sensor_i] % INSTANCEIO_BUFFER_RESIZE == 0) {
					unsigned int* tmp = (unsigned int*)calloc((size_t)output->nb_targets_covered[sensor_i] + INSTANCEIO_BUFFER_RESIZE, sizeof(unsigned int));
					assert(tmp);
					memcpy(tmp, output->targets_covered[sensor_i], output->nb_targets_covered[sensor_i]);
					free(output->targets_covered[sensor_i]);
					output->targets_covered[sensor_i] = tmp;
				}

				output->targets_covered[sensor_i][output->nb_targets_covered[sensor_i]++] = i;
			}
		}
	}

	fclose(f);	// Fermeture du fichier d'instance

	return 0;
}

int SaveInstanceAsLP(Instance* instance, const char* dstFile)
{
	FILE* f = fopen(dstFile, "w");

	unsigned int i, j;	// It�rateurs

	{	// re-formatting
		fprintf(f, "Minimize\n z: ");
		fprintf(f, "%u x%d", instance->sensor_costs[0], 1);
		for (i = 1; i < instance->nb_sensors; i++)
			fprintf(f, " + %u x%d", instance->sensor_costs[i], i + 1);
		fprintf(f, "\nSubject To\n");

		char first = 1;
		for (i = 0; i < instance->nb_targets; i++) {
			fprintf(f, " cible%d: ", i + 1);
			first = 1;
			for (j = 0; j < instance->nb_sensors; j++)
				if (instance->targets_sensors[i][j]) {
					if (!first)
						fprintf(f, " + ");
					fprintf(f, "x%d", j + 1);
					first = 0;
				}
			fprintf(f, " >= 1\n");
		}

		fprintf(f, "Binaries\n");
		for (i = 1; i <= instance->nb_sensors; i++)
			fprintf(f, " x%d\n", i);
		fprintf(f, "End\n");
	}

	fclose(f);

	return 0;
}
