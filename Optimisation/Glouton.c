#include "Glouton.h"

#include <stdlib.h>
#include <assert.h>
#include "util.h"

unsigned int glouton_heuristique(GloutonInfo* info) {
	unsigned int sensor_i = 0, i, j, nb;
	float max = 0, eval;

	for (i = 0; i < info->instance->nb_sensors; i++) {
		if (info->sensors_enabled[i]) continue;

		eval = 0;
		nb = info->instance->nb_targets_covered[i];
		for (j = 0; j < nb; j++)
			eval += info->targets_not_covered[info->instance->targets_covered[i][j]];
		// for (j = 0; j < info->instance->nb_targets; j++)
		// 	if (info->instance->targets_sensors[j][i])
		// 		eval += info->targets_not_covered[j];
		eval /= info->instance->sensor_costs[i];

		if (eval > max) {
			max = eval;
			sensor_i = i;
		}
	}

	return sensor_i;
}

unsigned int glouton_aleatoire(GloutonInfo* info)
{
	return rand() % info->instance->nb_sensors;
}

unsigned int Glouton(Instance* instance, unsigned int (*f)(GloutonInfo*), unsigned int** sensors, unsigned int* nb_sensors) {
	if (!instance) return -1;
	if (!f) return -1;

	unsigned int i, j;	// It�rateurs

	// Liste des cibles non-couvertes
	unsigned char* targets_not_covered = (unsigned char*)calloc((size_t)instance->nb_targets + 1, sizeof(unsigned char));
	// Lsite des capteurs activ�s
	unsigned char* sensors_enabled = (unsigned char*)calloc((size_t)instance->nb_sensors + 1, sizeof(unsigned char));

	assert(targets_not_covered);
	assert(sensors_enabled);

	GloutonInfo info;
	info.instance = instance;
	info.targets_not_covered = targets_not_covered;
	info.sensors_enabled = sensors_enabled;

	// RAZ
	targets_not_covered[instance->nb_targets] = 0;
	sensors_enabled[instance->nb_sensors] = 0;

	for (i = 0; i < instance->nb_targets; i++)
		targets_not_covered[i] = 1;

	unsigned int sensor_i = 0;
	while (find(targets_not_covered, instance->nb_targets, 1) != instance->nb_targets) {
		// Heuristique
		sensor_i = (*f)(&info);
		assert(sensor_i < instance->nb_sensors);

		// Update
		sensors_enabled[sensor_i] = 1;	// Capteur � l'index sensor_i activ�
		for (i = 0; i < instance->nb_targets_covered[sensor_i]; i++)	// Les cibles couvertes par le capteur sensor_i sont maintenant couvertes
			targets_not_covered[instance->targets_covered[sensor_i][i]] = 0;
	}

	if (nb_sensors && sensors) {
		*nb_sensors = sum(sensors_enabled, instance->nb_sensors);	// D�termination du nombre de capteurs activ�s
		*sensors = (unsigned int*)calloc(*nb_sensors, sizeof(unsigned int));	// Indices des capteurs activ�s
		assert(*sensors);

		for (i = 0, j = 0; i < instance->nb_sensors; i++)	// Compl�tion du tableau des capteurs activ�s
			if (sensors_enabled[i])
				(*sensors)[j++] = i;
	}

	unsigned int cost = 0;
	for (i = 0; i < instance->nb_sensors; i++)
		cost += sensors_enabled[i] * instance->sensor_costs[i];

	free(targets_not_covered);
	free(sensors_enabled);

	return cost;
}


unsigned int GloutonAmeliore(Instance* instance, unsigned int (*f)(GloutonInfo*), unsigned int** sensors, unsigned int* nb_sensors) {
	if (!instance) return -1;
	if (!f) return -1;

	unsigned int i, j, k;	// It�rateurs

	unsigned char* targets_not_covered = (unsigned char*)calloc((size_t)instance->nb_targets, sizeof(unsigned char));
	unsigned char* sensors_enabled = (unsigned char*)calloc((size_t)instance->nb_sensors, sizeof(unsigned char));

	assert(targets_not_covered);
	assert(sensors_enabled);

	GloutonInfo info;
	info.instance = instance;
	info.targets_not_covered = targets_not_covered;
	info.sensors_enabled = sensors_enabled;

	// RAZ
	for (i = 0; i < instance->nb_targets; i++)
		targets_not_covered[i] = 1;

	unsigned int sensor_i = 0;
	while (find(targets_not_covered, instance->nb_targets, 1) != instance->nb_targets) {
		// Heuristique
		sensor_i = (*f)(&info);
		assert(sensor_i < instance->nb_sensors);

		// Update
		sensors_enabled[sensor_i] = 1;	// Capteur � l'index sensor_i activ�
		for (i = 0; i < instance->nb_targets_covered[sensor_i]; i++)	// Les cibles couvertes par le capteur sensor_i sont maintenant couvertes
			targets_not_covered[instance->targets_covered[sensor_i][i]] = 0;
	}

	// Remove all useless sensors
	for (i = 0; i < instance->nb_sensors; i++) {
		if (!sensors_enabled[i]) continue;
	
		unsigned char useful = 0;
		for (j = 0; j < instance->nb_targets_covered[i]; j++) {
			unsigned int target = instance->targets_covered[i][j];
			unsigned char already_covered = 0;
			for (k = 0; k < instance->nb_sensors_covering[target]; k++)
				if (instance->sensors_covering[target][k] != i &&
					sensors_enabled[instance->sensors_covering[target][k]]) {
					already_covered = 1;
					break;
				}
			if (!already_covered) {
				useful = 1;
				break;
			}
		}
	
		if (!useful)
			sensors_enabled[i] = 0;
	}

	if (nb_sensors && sensors) {
		*nb_sensors = sum(sensors_enabled, instance->nb_sensors);	// D�termination du nombre de capteurs activ�s
		*sensors = (unsigned int*)calloc(*nb_sensors, sizeof(unsigned int));	// Indices des capteurs activ�s
		assert(*sensors);

		for (i = 0, j = 0; i < instance->nb_sensors; i++)	// Compl�tion du tableau des capteurs activ�s
			if (sensors_enabled[i])
				(*sensors)[j++] = i;
	}

	unsigned int cost = 0;
	for (i = 0; i < instance->nb_sensors; i++)
		cost += sensors_enabled[i] * instance->sensor_costs[i];

	free(targets_not_covered);
	free(sensors_enabled);

	return cost;
}