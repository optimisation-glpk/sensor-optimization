#include "Genetics.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include "util.h"

struct GeneticsInstance {
	void* userdata;											// Donn�es utilisateur
	int (*initializer)(struct GeneticsSolution**, unsigned int, void*);	// Fonction d'initialisation de la population
	unsigned int (*eval)(struct GeneticsSolution*, void*);			// Fonction d'�valuation des solutions
	void (*repair)(struct GeneticsSolution*, void*);		// Fonction de r�paration dse solutions
	void (*free)(struct GeneticsSolution*, void*);			// Fonction pour lib�rer la m�moire � allouer pour la solution
	int (*stop)(void*);		// Fonction condition d'arr�t

	unsigned int nb_solutions;		// Nombre d'individus
	unsigned int nb_solutions_selected;	// Nombre d'individus s�lectionn�s � chaque g�n�ration
	float prob_mix, prob_mutation;		// Probabilit�s de croisement et mutation
	struct GeneticsSolution** solutions;	// Individus de la population (ou solutions)
};

GeneticsInstance* Genetics_CreateInstance(unsigned int nb_solutions, unsigned int nb_solutions_selected)
{
	if (nb_solutions <= 0) return 0;

	GeneticsInstance* instance = (GeneticsInstance*)malloc(sizeof(GeneticsInstance));
	assert(instance);
	instance->userdata = 0;
	instance->initializer = 0;
	instance->eval = 0;
	instance->repair = 0;
	instance->free = 0;

	instance->prob_mix = 0.85f;
	instance->prob_mutation = 0;

	instance->nb_solutions = nb_solutions;
	instance->nb_solutions_selected = nb_solutions_selected;
	instance->solutions = (GeneticsSolution**)calloc(instance->nb_solutions, sizeof(GeneticsSolution*));
	assert(instance->solutions);

	unsigned int i;
	for (i = 0; i < nb_solutions; i++) {
		instance->solutions[i] = (GeneticsSolution*)malloc(sizeof(GeneticsSolution));
		assert(instance->solutions[i]);
		instance->solutions[i]->instance = instance;
		instance->solutions[i]->nb_chromosomes = 0;
		instance->solutions[i]->chromosomes = 0;
	}

	return instance;
}

int Genetics_SetUserData(GeneticsInstance* instance, void* userData)
{
	if (!instance) return 1;
	instance->userdata = userData;
	return 0;
}

int Genetics_SetInitializer(GeneticsInstance* instance, int(*initializer)(GeneticsSolution**, unsigned int, void*))
{
	if (!instance) return 1;
	instance->initializer = initializer;
	return 0;
}

int Genetics_SetSolutionEvaluator(GeneticsInstance* instance, unsigned int(*eval)(GeneticsSolution*, void*))
{
	if (!instance) return 1;
	instance->eval = eval;
	return 0;
}

int Genetics_SetSolutionRepair(GeneticsInstance* instance, void(*repair)(GeneticsSolution*, void*))
{
	if (!instance) return 1;
	instance->repair = repair;
	return 0;
}

int Genetics_SetStopCondition(GeneticsInstance* instance, int(*stop)(void*))
{
	if (!instance) return 1;
	instance->stop = stop;
	return 0;
}

int Genetics_SetFreeSolution(GeneticsInstance* instance, void(*free)(GeneticsSolution*, void*))
{
	if (!instance) return 1;
	instance->free = free;
	return 0;
}

int Genetics_SetMixProbability(GeneticsInstance* instance, float mixProb)
{
	if (!instance) return 1;
	instance->prob_mix = mixProb;
	return 0;
}

int Genetics_SetMutationProbability(GeneticsInstance* instance, float mutationProb)
{
	if (!instance) return 1;
	instance->prob_mutation = mutationProb;
	return 0;
}


GeneticsSolution* CopySolution(GeneticsSolution* solution) {
	if (!solution) return 0;

	GeneticsSolution* res = (GeneticsSolution*)malloc(sizeof(GeneticsSolution));
	assert(res);

	res->instance = solution->instance;
	res->nb_chromosomes = solution->nb_chromosomes;
	res->chromosomes = (unsigned char*)calloc(res->nb_chromosomes, sizeof(unsigned char));
	assert(res->chromosomes);
	for (unsigned int i = 0; i < res->nb_chromosomes; i++)
		res->chromosomes[i] = solution->chromosomes[i];

	return res;
}

int contains_(unsigned int* a, unsigned int size, unsigned int value) {
	for (unsigned int i = 0; i < size; i++)
		if (a[i] == value)
			return 1;
	return 0;
}

unsigned int Genetics_Process(GeneticsInstance* instance, unsigned char** solution)
{
	if (!instance) return 1;
	if (!instance->initializer || !instance->eval || !instance->repair || !instance->free) return 1;

	unsigned int i, j;
	int i_, j_, iter = 50;
	if (instance->initializer(instance->solutions, instance->nb_solutions, instance->userdata)) {
		fprintf(stderr, "Failed to initialize the genetics instance\n");
		return -1;
	}

	unsigned char tmp;
	unsigned int K, best = 0, nb_total_solutions = instance->nb_solutions + instance->nb_solutions_selected;
	float sum_probs, prob;
	// Tableau r�gissant les scores de chaque individu
	struct SolutionScore {
		GeneticsSolution* solution;
		unsigned int score;
		float prob;
	}*scores = (struct SolutionScore*)calloc(nb_total_solutions, sizeof(struct SolutionScore)), tmp_score;
	assert(scores);
	if (solution) {
		*solution = (unsigned char*)calloc(instance->solutions[0]->nb_chromosomes, sizeof(unsigned char));
		assert(*solution);
	}

	float esp = instance->prob_mutation * instance->solutions[0]->nb_chromosomes;
	unsigned int nb_mutations = (esp - (int)esp < 0.5f) ? (unsigned int)esp : (unsigned int)esp + 1;
	unsigned int* mutations = (unsigned int*)calloc(nb_mutations, sizeof(unsigned int)), mutation;
	
	// Partie du tableau des scores d�di�e aux individus s�lectionn�s
	struct SolutionScore* selected = scores + instance->nb_solutions;

	// Remplissage du tableau des scores
	best = instance->eval(instance->solutions[0], instance->userdata);
	for (i = 0; i < instance->nb_solutions; i++) {
		scores[i].solution = CopySolution(instance->solutions[i]);
		scores[i].score = instance->eval(instance->solutions[i], instance->userdata);
		if (scores[i].score < best) {
			best = scores[i].score;
			if (solution) {
				for (j = 0; j < scores[i].solution->nb_chromosomes; j++)
					(*solution)[j] = scores[i].solution->chromosomes[j];
			}
		}
	}

	while ((instance->stop) ? instance->stop(instance->userdata) : iter-- > 0) {
		// -------- Selection - Reproduction ----------
		sum_probs = 
			0;
		// Calcul des probabilit�s de chaque individu
		for (i = 0; i < instance->nb_solutions; i++) {
			scores[i].score = instance->eval(scores[i].solution, instance->userdata);
			scores[i].prob = 1.f / (float_rand(0.95f, 1.05f) * scores[i].score);
			sum_probs += scores[i].prob;
		}
		for (i = 0; i < instance->nb_solutions; i++)
			scores[i].prob /= sum_probs;

		// S�lection des individus probabiliste
		for (i = 0; i < instance->nb_solutions_selected; i++) {
			prob = float_rand(0, 1);
			sum_probs = 0.f;
			for (j = 0; j < instance->nb_solutions; j++) {
				sum_probs += scores[j].prob;
				if (prob < sum_probs)
					break;
			}
			selected[i].solution = CopySolution(scores[j].solution);	// L'individu s�lectionn� est copi� dans la partie d�di� aux individus s�lectionn�
		}

		// ------------- Croisement ----------------

		for (i = 0; i < instance->nb_solutions_selected; i += 2) {
			if (instance->nb_solutions_selected - i <= 1) break;

			if (float_rand(0, 1) < instance->prob_mix) {	// Si l'indiividu doit �tre crois�, on le croise avec le suivant
				K = (rand() % (selected[i].solution->nb_chromosomes - 1)) + 1;
				for (j = K; j < selected[i].solution->nb_chromosomes; j++) {
					tmp = selected[i].solution->chromosomes[j];
					selected[i].solution->chromosomes[j] = selected[i + 1].solution->chromosomes[j];
					selected[i + 1].solution->chromosomes[j] = tmp;
				}
			}
		}

		// ------------- Mutation -------------------

		/*
		for (i = 0; i < instance->nb_solutions_selected; i++)
			for (j = 0; j < selected[i].solution->nb_chromosomes; j++)
				if (float_rand(0, 1) < instance->prob_mutation)
					selected[i].solution->chromosomes[j] = !selected[i].solution->chromosomes[j];
		*/
		for (i = 0; i < instance->nb_solutions_selected; i++) {
			for (j = 0; j < nb_mutations; j++)
				mutations[j] = -1;
			for (j = 0; j < nb_mutations; j++) {
				do {
					mutation = rand() % selected[i].solution->nb_chromosomes;
				} while (contains_(mutations, nb_mutations, mutation));
				selected[i].solution->chromosomes[mutation] = !selected[i].solution->chromosomes[mutation];
			}
		}

		// -------- R�paration + Evaluation ----------------

		for (i = 0; i < instance->nb_solutions_selected; i++) {
			instance->repair(selected[i].solution, instance->userdata);
			selected[i].score = instance->eval(selected[i].solution, instance->userdata);
		}

		// ---- Tri des solutions par score croissant --------

		// Les meilleurs individus se retrouvent en d�but de liste et les individus non s�lectionn�s pour la survie 
		// se retrouvent � la fi du tableau, dans la partie d�di�e aux individus s�lectionn�s pour la reproduction
		for (i_ = nb_total_solutions - 1; i_ >= 0; i_--)	// tri � bulles
			for (j_ = 0; j_ < i_; j_++)
				if (scores[j_ + 1].score < scores[j_].score) {
					tmp_score = scores[j_];
					scores[j_] = scores[j_ + 1];
					scores[j_ + 1] = tmp_score;
				}

		// printf("Scores: ");
		// for (i = 0; i < nb_total_solutions; i++)
		// 	printf("%u ", scores[i].score);
		// printf("\n\n");

		// --------- Enregistrement du r�sultat --------

		if (scores[0].score < best) {
			best = scores[0].score;
			if (solution) {
				for (j = 0; j < scores[0].solution->nb_chromosomes; j++)
					(*solution)[j] = scores[0].solution->chromosomes[j];
			}
		}

		// --------- Selection - Survie --------------

		// Suppressions des individus dans la partie "s�lection" du tableaud es scores
		for (i = 0; i < instance->nb_solutions_selected; i++) {
			instance->free(selected[i].solution, instance->userdata);
			free(selected[i].solution);
			selected[i].solution = 0;
		}
	}

	for (i = 0; i < instance->nb_solutions; i++) {
		instance->free(scores[i].solution, instance->userdata);
		free(scores[i].solution);
		scores[i].solution = 0;
	}
	free(scores);
	free(mutations);

	for (i = 0; i < instance->nb_solutions; i++)
		instance->free(instance->solutions[i], instance->userdata);

	return best;
}

int Genetics_FreeInstance(GeneticsInstance** instance)
{
	if (!instance || !*instance) return 1;

	for (unsigned int i = 0; i < (*instance)->nb_solutions; i++) {
		free((*instance)->solutions[i]);
		(*instance)->solutions[i] = 0;
	}

	if ((*instance)->solutions) {
		free((*instance)->solutions);
		(*instance)->solutions = 0;
	}

	free(*instance);
	*instance = 0;

	return 0;
}
