#include "Instance.h"

#include <stdlib.h>

Instance* AllocInstance() {
	Instance* out = (Instance*)malloc(sizeof(Instance));

	if (!out) return 0;

	out->nb_sensors = 0;
	out->nb_targets = 0;
	out->sensor_costs = 0;
	out->targets_covered = 0;
	out->nb_targets_covered = 0;
	out->sensors_covering = 0;
	out->nb_sensors_covering = 0;
	out->targets_sensors = 0;

	return out;
}

int FreeInstance(Instance** instance) {
	if (!instance) return 1;
	if (!*instance) return 1;

	unsigned int i;
	if ((*instance)->sensor_costs) {
		free((*instance)->sensor_costs);
		(*instance)->sensor_costs = 0;
	}

	if ((*instance)->targets_covered) {
		for (i = 0; i < (*instance)->nb_sensors; i++)
			if ((*instance)->targets_covered[i]) {
				free((*instance)->targets_covered[i]);
				(*instance)->targets_covered[i] = 0;
			}
		free((*instance)->targets_covered);
		(*instance)->targets_covered = 0;
	}

	if ((*instance)->nb_targets_covered) {
		free((*instance)->nb_targets_covered);
		(*instance)->nb_targets_covered = 0;
	}

	if ((*instance)->sensors_covering) {
		for (i = 0; i < (*instance)->nb_targets; i++)
			if ((*instance)->sensors_covering[i]) {
				free((*instance)->sensors_covering[i]);
				(*instance)->sensors_covering[i] = 0;
			}
		free((*instance)->sensors_covering);
		(*instance)->sensors_covering= 0;
	}

	if ((*instance)->nb_sensors_covering) {
		free((*instance)->nb_sensors_covering);
		(*instance)->nb_sensors_covering = 0;
	}

	if ((*instance)->targets_sensors) {
		for (i = 0; i < (*instance)->nb_targets; i++)
			if ((*instance)->targets_sensors[i]) {
				free((*instance)->targets_sensors[i]);
				(*instance)->targets_sensors[i] = 0;
			}
		free((*instance)->targets_sensors);
		(*instance)->targets_sensors = 0;
	}

	free(*instance);
	*instance = 0;

	return 0;
}