#include "CSV.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

CSVDoc* CSV_CreateDoc(char** columns, unsigned int nb_columns)
{
	if (!columns || nb_columns <= 0) return 0;

	unsigned int i, len;
	for (i = 0; i < nb_columns; i++)
		if (!columns[i]) return 0;

	CSVDoc* doc = (CSVDoc*)calloc(1, sizeof(CSVDoc));
	assert(doc);
	doc->nb_columns = nb_columns;
	doc->columns = (char**)calloc(nb_columns, sizeof(char*));
	assert(doc->columns);
	for (i = 0; i < nb_columns; i++) {
		for (len = 0; columns[i][len] != 0; len++);
		doc->columns[i] = (char*)calloc((size_t)len + 1, sizeof(char));
		assert(doc->columns[i]);
		doc->columns[i][len] = 0;
		memcpy(doc->columns[i], columns[i], len);
	}
	doc->_firstRow = 0;

	return doc;
}

// void _CSV_AddRow(CSVRow** current, CSVRow* toInsert) {
// 	if (!current) return;
// 
// 	if (!(*current))
// 		*current = toInsert;
// 	else
// 		_CSV_AddRow(&(*current)->next, toInsert);
// }

CSVRow* CSV_Push(CSVDoc* doc, void* data) {
	if (!doc) return 0;

	CSVRow* row = (CSVRow*)malloc(sizeof(CSVRow));
	assert(row);
	row->doc = doc;
	row->data = data;
	row->next = 0;
	// _CSV_AddRow(&doc->_firstRow, row);

	CSVRow** node = &doc->_firstRow;
	while (*node)
		node = &(*node)->next;
	*node = row;

	return 0;
}

CSVRow* CSV_InsertFirst(CSVDoc* doc, void* data) {
	if (!doc) return 0;

	CSVRow* row = (CSVRow*)malloc(sizeof(CSVRow));
	assert(row);
	row->doc = doc;
	row->data = data;
	row->next = 0;

	if (doc->_firstRow)
		row->next = doc->_firstRow;
	doc->_firstRow = row;

	// _CSV_AddRow(&doc->_firstRow, row);

	return 0;
}

int _CSV_WriteDoc(FILE* f, CSVRow* row, void(*printer)(FILE*, void*, unsigned int)) {
	if (!row) return 0;

	unsigned int i;
	for (i = 0; i < row->doc->nb_columns; i++) {
		printer(f, row->data, i);
		fprintf(f, ";");
	}
	fprintf(f, "\n");

	return _CSV_WriteDoc(f, row->next, printer);
}

int CSV_WriteDoc(CSVDoc* doc, const char* filepath, void(*printer)(FILE*, void*, unsigned int)) {
	if (!printer) return 1;

	FILE* f = fopen(filepath, "w");
	
	if (!f) return 1;

	unsigned int i;
	for (i = 0; i < doc->nb_columns; i++)
		fprintf(f, "%s;", doc->columns[i]);
	fprintf(f, "\n");

	CSVRow* row = doc->_firstRow;
	while (row) {
		for (i = 0; i < doc->nb_columns; i++) {
			printer(f, row->data, i);
			fprintf(f, ";");
		}
		fprintf(f, "\n");
		row = row->next;
	}

	fclose(f);

	return 0;
}

int _CSV_FreeRow(CSVRow* row, void (*freeData)(void*)) {
	if (!row) return 0;

	if (freeData)
		freeData(row->data);
	_CSV_FreeRow(row->next, freeData);
	free(row);

	return 0;
}

int CSV_FreeDoc(CSVDoc** doc, void (*freeData)(void*))
{
	if (!doc) return 1;
	CSVDoc* _doc = *doc;
	if (!_doc) return 1;

	unsigned int i;
	for (i = 0; i < _doc->nb_columns; i++) {
		free(_doc->columns[i]);
		_doc->columns[i] = 0;
	}

	_CSV_FreeRow(_doc->_firstRow, freeData);

	free(*doc);
	*doc = 0;

	return 0;
}
