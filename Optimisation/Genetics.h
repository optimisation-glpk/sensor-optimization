#pragma once

#ifndef __GENETICS_DEF_INCLUDED
#define __GENETICS_DEF_INCLUDED

typedef struct GeneticsInstance GeneticsInstance;

typedef struct GeneticsSolution {
	unsigned int nb_chromosomes;	// Nombre de chromosomes (ou capteurs)
	unsigned char* chromosomes;		// Chromosomes (ou capteurs)
	GeneticsInstance* instance;	// Population containing this solution
} GeneticsSolution;

/**
* \brief Cr�er une instance pour ex�cuter l'algorithme g�n�tique
* \param nb_solutions: Nombre de solutions � chaque g�n�ration
* \param nb_solutions_selected: Nombre de solutions s�lectionn�es � chaque g�n�ration
* \return Instance d'ex�cution pour algorithme g�n�tique
*/
GeneticsInstance* Genetics_CreateInstance(unsigned int nb_solutions, unsigned int nb_solutions_selected);

/**
* \brief D�finit les donn�es utilisateur utilis�es pendant le calcul de la solution
* \param instance: Instance d'ex�cution
* \param userData: Pointeur sur les donn�es utilisateurs
* \return 0 si r�ussi, 1 sinon
*/
int Genetics_SetUserData(GeneticsInstance* instance, void* userData);

/**
* \brief D�finit la fonction d'initialisation de la population
* \param instance: Instance d'ex�cution
* \param userData: Fonction d'initialisation de la population
* \return 0 si r�ussi, 1 sinon
*/
int Genetics_SetInitializer(GeneticsInstance* instance, int(*initializer)(GeneticsSolution**, unsigned int, void*));

/**
* \brief D�finit la fonction d'�valution des solutions
* \param instance: Instance d'ex�cution
* \param userData: Fonction d'�valution
* \return 0 si r�ussi, 1 sinon
*/
int Genetics_SetSolutionEvaluator(GeneticsInstance* instance, unsigned int(*eval)(GeneticsSolution*, void*));

/**
* \brief D�finit la fonction de r�paration des solutions
* \param instance: Instance d'ex�cution
* \param userData: Fonction de r�paration
* \return 0 si r�ussi, 1 sinon
*/
int Genetics_SetSolutionRepair(GeneticsInstance* instance, void(*repair)(GeneticsSolution*, void*));

/**
* \brief D�finit la condition d'arr�t
* \param instance: Instance d'ex�cution
* \param stop: Fonction avec condition d'arr�t
* \return 0 si r�ussi, 1 sinon
*/
int Genetics_SetStopCondition(GeneticsInstance* instance, int(*stop)(void*));

/**
* \brief D�finit la fonction pour lib�rer l'espace allou�e par l'utilisateur � chaque solution
* \param instance: Instance d'ex�cution
* \param userData: Fonction d'�valution
* \return 0 si r�ussi, 1 sinon
*/
int Genetics_SetFreeSolution(GeneticsInstance* instance, void(*free)(GeneticsSolution*, void*));

/**
* \brief D�finit la probabilt� de croisement des individus
* \param instance: Instance d'ex�cution
* \param userData: Fonction d'�valution
* \return 0 si r�ussi, 1 sinon
*/
int Genetics_SetMixProbability(GeneticsInstance* instance, float mixProb);

/**
* \brief D�finit la probabilt� de mutation des individus
* \param instance: Instance d'ex�cution
* \param userData: Fonction d'�valution
* \return 0 si r�ussi, 1 sinon
*/
int Genetics_SetMutationProbability(GeneticsInstance* instance, float mutationProb);


/**
* \brief Calcule la solution du probl�me
* \param instance: Instance d'ex�cution
* \param nb_iterations: Nombre d'it�ration � r�aliser
* \param solution [out]: Pointeur sur la solution
* \return Valeur objectif
*/
unsigned int Genetics_Process(GeneticsInstance* instance, unsigned char** solution);

/**
* \brief Lib�re l'instance d'ex�cution
* \param instance: Instance d'ex�cution
* \return 1 si r�ussi, 0 sinon
*/
int Genetics_FreeInstance(GeneticsInstance** instance);

#endif