#pragma once

#ifndef __TIMER_DEF_INCLUDED
#define __TIMER_DEF_INCLUDED

typedef struct Timer* Timer;

/**
* \brief Cr�e un minuteur
* \return Minuteur
*/
Timer tcreate();

/**
* \brief D�marre le minuteur
* \param timer: Minuteur
* \param time: D�compte
* \return 0 si le chonom�tre est lanc�, 1 sinon
*/
int tstart(Timer timer, float time);

/**
* \brief Donne le temps restant
* \param timer: Minuteur
* \return Secondes restantes, -1 si erreur
*/
float tremaining(Timer timer);

/**
* \brief D�termine si le minuteur a fini le d�compte
* \param timer: Minuteur
* \return 1 si termin�, 0 sinon
*/
int tfinished(Timer timer);

/**
* \brief Lib�re le minuteur
* \param: Pointeur sur minuteur
* \return 0 si r�ussi, 1 sinon
*/
int tfree(Timer* timer);

#endif