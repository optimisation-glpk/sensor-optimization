#include "util.h"

#include <stdlib.h>

int sum(unsigned char* array, unsigned int size) {
	unsigned int sum, i;
	for (sum = 0, i = 0; i < size; sum += array[i++]);
	return sum;
}

unsigned int find(unsigned char* array, unsigned int size, unsigned char value) {
	unsigned int i;
	for (i = 0; i < size; i++)
		if (array[i] == value) break;
	return i;
}

float float_rand(float min, float max) {
	return min + (rand() / (float)RAND_MAX) * (max - min);
}
