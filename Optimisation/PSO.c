#include "PSO.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "util.h"

struct PSOInstance {
    unsigned int nb_components;		// Nombre de composantes dans l'espace des solutions
    unsigned int swarm_size;		// Quantit� de particules
    PSOParticle** swarm;			// Essaim ou particules
    float inertia, self_rate, swarm_rate, learning_rate;	// Ratios de progression
    unsigned int (*eval)(unsigned char*, void*);	// Fonction d'�valuation
    void (*init_pos)(PSOParticle**, unsigned int, void*);	// Fonction d'initialisation de la position des particules
    void (*stateToSolution)(PSOParticle*, unsigned char*, void*);	// Fonction de transformation d'un �tat � une solution
    int (*stop)(void*);	// Fonction avec conditoin d'arr�t
    void* userdata;		// Donn�es utilisateurs
};

PSOInstance* PSO_CreateInstance(unsigned int nb_particles, unsigned int nb_components)
{
    if (nb_components <= 0 || nb_particles <= 0) return 0;

    PSOInstance* instance = (PSOInstance*)malloc(sizeof(PSOInstance));
    assert(instance);

    instance->inertia = 0.33f;
    instance->self_rate = 0.33f;
    instance->swarm_rate = 0.33f;
    instance->learning_rate = 1;
    instance->nb_components = nb_components;

    instance->eval = 0;
    instance->init_pos = 0;
    instance->stateToSolution = 0;

    instance->swarm_size = nb_particles;
    instance->swarm = (PSOParticle**)calloc(nb_particles, sizeof(PSOParticle*));
    assert(instance->swarm);

    unsigned int i;
    for (i = 0; i < nb_particles; i++) {
        instance->swarm[i] = (PSOParticle*)malloc(sizeof(PSOParticle));
        assert(instance->swarm[i]);

        instance->swarm[i]->instance = instance;
        instance->swarm[i]->nb_components = nb_components;
        instance->swarm[i]->position = (float*)calloc(nb_components, sizeof(float));
        instance->swarm[i]->velocity = (float*)calloc(nb_components, sizeof(float));

        assert(instance->swarm[i]->position);
        assert(instance->swarm[i]->velocity);
    }
    
    return instance;
}

PSOParticle* PSO_CopyParticle(PSOParticle* particle)
{
    if (!particle) return 0;

    PSOParticle* res = (PSOParticle*)malloc(sizeof(PSOParticle));
    assert(res);

    res->instance = particle->instance;
    res->nb_components = particle->nb_components;
    res->position = (float*)calloc(particle->nb_components, sizeof(float));
    assert(res->position);
    res->velocity = (float*)calloc(particle->nb_components, sizeof(float));
    assert(res->velocity);

    unsigned int i;
    for (i = 0; i < particle->nb_components; i++) {
        res->position[i] = particle->position[i];
        res->velocity[i] = particle->velocity[i];
    }

    return res;
}

int PSO_SetInertia(PSOInstance* instance, float w)
{
    if (!instance) return 1;
    instance->inertia = w;
    return 0;
}

int PSO_SetSelfRate(PSOInstance* instance, float r)
{
    if (!instance) return 1;
    instance->self_rate = r;
    return 0;
}

int PSO_SetSwarmRate(PSOInstance* instance, float r)
{
    if (!instance) return 1;
    instance->swarm_rate = r;
    return 0;
}

int PSO_SetLearningRate(PSOInstance* instance, float r)
{
    if (!instance) return 1;
    instance->learning_rate = r;
    return 0;
}

int PSO_SetStateToSolution(PSOInstance* instance, void (*stateToSolution)(PSOParticle*, unsigned char*, void*))
{
    if (!instance || !stateToSolution) return 1;
    instance->stateToSolution = stateToSolution;
    return 0;
}

int PSO_SetEvaluationFunction(PSOInstance* instance, unsigned int(*eval)(unsigned char*, void*))
{
    if (!instance || !eval) return 1;
    instance->eval = eval;
    return 0;
}

int PSO_SetPosInitFunction(PSOInstance* instance, void(*pos_init)(PSOParticle**, unsigned int, void*))
{
    if (!instance || !pos_init) return 1;
    instance->init_pos = pos_init;
    return 0;
}

int PSO_SetStopCondition(PSOInstance* instance, int (*stop)(void*))
{
    if (!instance || !stop) return 1;
    instance->stop = stop;
    return 0;
}

int PSO_SetUserData(PSOInstance* instance, void* userdata)
{
    if (!instance) return 1;
    instance->userdata = userdata;
    return 0;
}

unsigned int PSO_Process(PSOInstance* instance, unsigned char** solution)
{
    if (!instance) return -1;
    if (!instance->eval || !instance->init_pos || !instance->stateToSolution) return -1;

    unsigned int i, j, score;
    float r1, r2;
    int iter = 50;

    // Initialisation de la position
    instance->init_pos(instance->swarm, instance->swarm_size, instance->userdata);

    struct ParticleScore {
        PSOParticle* particle;
        float* best_pos;
        unsigned int best_score;
    }*scores = (struct ParticleScore*)calloc(instance->swarm_size, sizeof(struct ParticleScore));
    assert(scores);
    unsigned char* s = (unsigned char*)calloc(instance->nb_components, sizeof(unsigned char));
    assert(s);

    struct ParticleScore* swarm_best = scores;

    for (i = 0; i < instance->swarm_size; i++) {
        scores[i].particle = instance->swarm[i];

        // Meilleur score = score initial
        instance->stateToSolution(scores[i].particle, s, instance->userdata);
        scores[i].best_score = instance->eval(s, instance->userdata);

        // Initialisation des composantes dans l'espace
        scores[i].best_pos = (float*)calloc(instance->swarm[i]->nb_components, sizeof(float));
        assert(scores[i].best_pos);

        for (j = 0; j < instance->nb_components; j++) {
            // Meilleur position = position initial
            scores[i].best_pos[j] = instance->swarm[i]->position[j];
            // Initialisation de la vitesse (vitesse al�atoire)
            instance->swarm[i]->velocity[j] = float_rand(0, 1);
        }
        
        // Meilleur score de l'essaim
        if (swarm_best->best_score > scores[i].best_score)
            swarm_best = scores + i;
    }

    while ((instance->stop) ? instance->stop(instance->userdata) : iter --> 0) {
        for (i = 0; i < instance->swarm_size; i++) {
            for (j = 0; j < instance->nb_components; j++) {
                // Coefficients al�atoires
                r1 = float_rand(0, 1);
                r2 = float_rand(0, 1);

                // V_t+1 = w * V_t + r1 * p1 * (p_best - x)  + r2 * p2 * (s_best - x)
                // p_best : meilleur de la particule
                // s_best : meilleur de l'essaim
                scores[i].particle->velocity[j] = instance->inertia * scores[i].particle->velocity[j] +
                    r1 * instance->self_rate * (scores[i].best_pos[j] - scores[i].particle->position[j]) +
                    r2 * instance->swarm_rate * (swarm_best->best_pos[j] - scores[i].particle->position[j]);

                scores[i].particle->position[j] += instance->learning_rate * scores[i].particle->velocity[j];
            }

            instance->stateToSolution(scores[i].particle, s, instance->userdata);

            // Est-ce que la solution est meilleure que la meilleure solution trouv�e par la particule
            if ((score = instance->eval(s, instance->userdata)) < scores[i].best_score) {
                scores[i].best_score = score;
                for (j = 0; j < instance->nb_components; j++)
                    scores[i].best_pos[j] = scores[i].particle->position[j];

                // Est-ce que la solution est meilleure de l'essaim
                if (score < swarm_best->best_score)
                    swarm_best = scores + i;
            }
        }
    }

    score = swarm_best->best_score;

    if (solution) {
        *solution = (unsigned char*)calloc(instance->nb_components, sizeof(unsigned char));
        assert(*solution);
        PSOParticle* p = PSO_CopyParticle(swarm_best->particle);
        for (i = 0; i < instance->nb_components; i++)
            p->position[i] = swarm_best->best_pos[i];
        instance->stateToSolution(p, s, instance->userdata);
        PSO_FreeParticle(&p);
        for (i = 0; i < instance->nb_components; i++)
            (*solution)[i] = s[i];
    }

    free(s);
    for (i = 0; i < instance->swarm_size; i++)
        free(scores[i].best_pos);
    free(scores);

    return score;
}

int PSO_FreeInstance(PSOInstance** instance)
{
    if (!instance || !*instance) return 1;

    if ((*instance)->swarm) {
        unsigned int i;
        for (i = 0; i < (*instance)->swarm_size; i++) {
            if ((*instance)->swarm[i])
                PSO_FreeParticle((*instance)->swarm + i);
        }

        free((*instance)->swarm);
        (*instance)->swarm = 0;
    }

    free(*instance);
    *instance = 0;

    return 0;
}

int PSO_FreeParticle(PSOParticle** particle)
{
    if (!particle || !*particle) return 1;

    if ((*particle)->position) {
        free((*particle)->position);
        (*particle)->position = 0;
    }
    if ((*particle)->velocity) {
        free((*particle)->velocity);
        (*particle)->velocity = 0;
    }

    free(*particle);
    *particle = 0;

    return 0;
}
